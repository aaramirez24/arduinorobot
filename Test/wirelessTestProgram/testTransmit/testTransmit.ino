/* YourDuinoStarter Example: nRF24L01 Transmit 
 - WHAT IT DOES: Transmits String to reciever

 - CONNECTIONS: nRF24L01 Modules:
   1 - GND
   2 - VCC 3.3V !!! NOT 5V
   3 - CE to Arduino pin 7
   4 - CSN to Arduino pin 8
   5 - SCK to Arduino pin 13
   6 - MOSI to Arduino pin 11
   7 - MISO to Arduino pin 12
   8 - UNUSED
   - 
  
 - V1.00 11/26/13
   Based on examples at http://www.bajdi.com/
   Questions: terry@yourduino.com */

/*-----( Import needed libraries )-----*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
/*-----( Declare Constants and Pin Numbers )-----*/
#define CE_PIN   7
#define CSN_PIN 8

// NOTE: the "LL" at the end of the constant is "LongLong" type
const uint64_t pipe = 0xE8E8F0F0E1LL; // Define the transmit pipe


/*-----( Declare objects )-----*/
RF24 radio(CE_PIN, CSN_PIN); // Create a Radio
/*-----( Declare Variables )-----*/
int sendBuff;

void setup()   /****** SETUP: RUNS ONCE ******/
{
  Serial.begin(115200);
  radio.begin();
  radio.openWritingPipe(pipe);
  Serial.println("Nrf24L01 Transmitter Starting");
}//--(end setup )---



void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  if (Serial.available() > 0) {
    sendBuff = Serial.read();
    radio.write(&sendBuff, 1);
    if (sendBuff == '\n') {
      Serial.println("Transmiting...");  
    }
  }

}//--(end main loop )---

/*-----( Declare User-written Functions )-----*/

//NONE
//*********( THE END )***********
