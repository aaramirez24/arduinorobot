/* YourDuinoStarter Example: nRF24L01 Receive Joystick values

 - WHAT IT DOES: Receives data from another transceiver with
   2 Analog values from a Joystick or 2 Potentiometers
   Displays received values on Serial Monitor
 - SEE the comments after "//" on each line below
 - CONNECTIONS: nRF24L01 Modules See:
 http://arduino-info.wikispaces.com/Nrf24L01-2.4GHz-HowTo
   1 - GND
   2 - VCC 3.3V !!! NOT 5V
   3 - CE to Arduino pin 7
   4 - CSN to Arduino pin 8
   5 - SCK to Arduino pin 13
   6 - MOSI to Arduino pin 11
   7 - MISO to Arduino pin 12
   8 - UNUSED
   
 - V1.00 11/26/13
   Based on examples at http://www.bajdi.com/
   Questions: terry@yourduino.com */

/*-----( Import needed libraries )-----*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
/*-----( Declare Constants and Pin Numbers )-----*/
#define CE_PIN   7
#define CSN_PIN 8

 #define ENABLE 3
 #define IN1 2
 #define IN2 4
 #define ENABLE2 3
 #define IN3 5
 #define IN4 6

// NOTE: the "LL" at the end of the constant is "LongLong" type
const uint64_t pipe = 0xE8E8F0F0E1LL; // Define the transmit pipe


/*-----( Declare objects )-----*/
RF24 radio(CE_PIN, CSN_PIN); // Create a Radio
/*-----( Declare Variables )-----*/
char wut[64];  // 1 element array holding wut readings
bool done = false;
int count = 0;

void setup()   /****** SETUP: RUNS ONCE ******/
{
  Serial.begin(115200);
  delay(1000);
  Serial.println("Nrf24L01 Receiver Starting");
  radio.begin();
  radio.openReadingPipe(1,pipe);
  radio.startListening();

  pinMode(ENABLE,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
}//--(end setup )---


void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  //int count = 0;
  if ( radio.available() )
  {
    //Serial.print("d");
    // Read the data payload until we've received everything
    //while (!done)
    //{
      // Fetch the data payload
      //radio.available();
      radio.read(&wut[count], sizeof(int));
      if (wut[count] == '\n') {
        done = true;
      }
      //Serial.print((char) wut[count]);
      count++;
    //}
  }
  if (done) {
    //digitalWrite(ENABLE,HIGH);
    analogWrite(ENABLE,170);
    Serial.println("Done Receiving...");
    //Serial.println(count);
    //String help = wut;
    //Serial.print(help);
    switch(wut[0]) {
      case 'a': Serial.println("left");
                break;
      case 'w': Serial.println("up");
                digitalWrite(IN1,LOW);
                digitalWrite(IN2,HIGH);
                digitalWrite(IN3,HIGH);
                digitalWrite(IN4,LOW);
                break;
      case 'd': Serial.println("right");
                digitalWrite(ENABLE,LOW);
                break;
      case 's': Serial.println("down");
                digitalWrite(IN1,HIGH);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,HIGH);
                break;
      default: Serial.println("invalid Move");
                break; 
    }
    count = 0;
    done = false;
  }
}//--(end main loop )---

/*-----( Declare User-written Functions )-----*/

//NONE
//*********( THE END )***********
